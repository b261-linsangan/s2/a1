package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Activity2 {
    public static void main(String[] args) {

        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        System.out.println("The first prime numbers is: " + primeNumbers[0]);

        //ArrayList
        ArrayList<String> friends = new ArrayList<>(
                Arrays.asList("John", "Jane", "Chloe", "Zoey")
        );
        System.out.println("Myfriends are: " + friends);

        //Hashmap
        HashMap<String, Integer> inventory = new HashMap<>() {
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };

        System.out.println("Our current inventory consists of: " + inventory);
    }
}
