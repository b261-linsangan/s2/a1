package com.zuitt.example;

import java.util.Scanner;

public class Activity1 {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        System.out.println("Input any year: ");
        int num1 = scan.nextInt();

        boolean isLeapYear = false;

        if (num1 % 4 == 0) {
            isLeapYear = true;

            if (num1 % 100 == 0) {
                if (num1 % 400 == 0) {
                    isLeapYear = true;
                }
            }
        }

        if(!isLeapYear) {
            System.out.println(num1 + " is not a leap year");
        } else {
            System.out.println(num1 + " is a leap year");
        }



    }
}
