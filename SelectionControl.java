package com.zuitt.example;

import java.util.Scanner;

public class SelectionControl {
    public static void main(String[] args) {

        //arithmethic = +, -, /, *, %
        //comparison = >, <, >=, <= !=
        //logical = &&, ||, !


        Scanner scan = new Scanner(System.in);

        System.out.println("Enter a number: ");
        int num1 = scan.nextInt();

        switch (num1) {
            case 1:
                System.out.println("Nice1");
                break;

            case 2:
                System.out.println("Nice2");
                break;

            case 3:
                System.out.println("Nice3");
                break;

            default:
                System.out.println("Nice4");
                break;
        }



    }
}
