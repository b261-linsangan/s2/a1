package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Array {
    public static void main(String[] args) {

        //Array
        // - containers of value of the same data type
        // - fixed sized, pre-defined size
        // - more rigid, once the size and data type are defined
        // - they can no longer be changed
        //Syntax: dataType[] identifier = new dataType[number of elements]
        //the values of the array is initialize to 0 or null

        int[] intArray = new int[3];
        intArray[0] = 1;
        intArray[1] = 1;
        intArray[2] = 1;

        System.out.println(intArray[2]);

        //declaring array with initialization
        //syntax:
        //datatype[] indentifier = {element};

        //multi-dimentional arrays
        //datatype[][] idenfitier = new datatype[rowLength][columnLength]
        String[][] classroom = new String [3][3];

        //first row
        classroom[0][0] = "name1";
        classroom[0][1] = "name2";
        classroom[0][2] = "name3";

        //second row
        classroom[1][0] = "name4";
        classroom[1][1] = "name5";
        classroom[1][2] = "name6";

        //third row
        classroom[2][0] = "name7";
        classroom[2][1] = "name8";
        classroom[2][2] = "name9";

//        System.out.println(Arrays.deepToString(classroom));

        //deepToString() for printing multi-dimensional array

        //ArrayLists
        //resizeable
        //syntax:
        //ArrayList<dataType> identifier = new ArrayList<dataType>();

//        ArrayList<String> students = new ArrayList<>();
//
//        students.add("John");
//        students.add("Paul");
//
//        System.out.println(students);

        ArrayList<String> students = new ArrayList<>(
                Arrays.asList("John", "paul")
        );

        System.out.println(students);

        //accessing an element
        System.out.println(students.get(1));

        //update
        students.set(1, "Gina");
        System.out.println(students);

        //adding an element on a speicific index,
        //isisingit niya lang
        students.add(1, "mike");
        System.out.println(students);

        //remove speicific element
        students.remove(1);
        System.out.println(students);

        //removing all elements
        students.clear();
        System.out.println(students);

        //getting the number of elements in an arraylist
        System.out.println(students.size());

        //integer
        ArrayList<Integer> num1 = new ArrayList<>(Arrays.asList(1,2,3));

        System.out.println(num1);

        //HashMaps
        //key also referred as fields
        //values can be access by fields
        //syntax:
        //HashMap<dataTypeField, dataTypeValue> identifier = new HashMap<dataTypeField, dataTypeValue>();

        HashMap<String, String> jobPost = new HashMap<>();

//        //add elements to Hashmap
//        jobPost.put("Student1", "Alice");
//        jobPost.put("Student2", "ron");
//        System.out.println(jobPost);

        //declaring and initializing
        HashMap<String, String> names = new HashMap<>() {
            {
                put("Student1", "Alice");
                put("Student2", "ronel");
            }
        };
        System.out.println(names);

        //accessing the hasmap
        System.out.println(names.get("Student1"));

        //updating an element
        names.replace("Student2", "Zz");
        System.out.println(names);

        //replace an existing key if the key argument is given with the same with exisiting
        names.put("Student2", "CC");
        System.out.println(names);

//        //remove an element
//        names.remove("Student1");
//        System.out.println(names);


        //remove all
//        names.clear();
//        System.out.println(names);

        System.out.println(names.keySet());
        System.out.println(names.values());

    }
}
